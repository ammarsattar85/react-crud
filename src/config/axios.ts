import axios, { AxiosError, AxiosInstance } from "axios";

const instance: AxiosInstance = axios.create({
    baseURL: "http://localhost:8080/api",
    headers: {
        "Content-Type": "application/json",
    },
    timeout: 0,
});

instance.interceptors.request.use(
    async (config) => {

        let token = localStorage.getItem("KEY_USER_ACCESS_TOKEN");
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }

        return config;
    },
    (error: AxiosError) => {
        return Promise.reject(error);
    }
);

instance.interceptors.response.use(
    (response) => response,
    (error) => {
        if (error.response && error.response.status === 403 in error.response) {

                // signOutHandler();
        }
        return Promise.reject(error);
    }
);

export default instance;
