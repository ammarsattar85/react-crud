import React, {useState} from 'react';
import {AuthApi} from "../../apis/auth.ts";

function Login() {
    const [state, setState] = useState<any>({
        email: "",
        password: "",
    });

    const signIn = async ()=> {
       await AuthApi.loginUser({email: state.email, password: state.password})
    }

    const handleChange = (evt: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) =>
        setState({
            ...state,
            [evt.target.name]: evt.target.value,
        });

    return (
        <div>
            <input name={"email"} type={"email"} placeholder={"Enter your email!"} onChange={handleChange}/>
            <input name={"password"} type={"password"} placeholder={"Enter your password!"} onChange={handleChange}/>
            <button onClick={signIn} > Submit </button>
        </div>
    );
}

export default Login;