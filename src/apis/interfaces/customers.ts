export interface Customer {
    name: string;
    isGold: boolean;
    phone: string;
}

export interface CustomerResp {
    data: {
        statusCode: number;
        data: Customer[]
    };
    status: number;
    statusText: string;
}

export interface CreateCustomerResp {
    data: {
        statusCode: number;
        data: Customer
    };
    status: number;
    statusText: string;
}