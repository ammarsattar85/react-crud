export interface LoginUserPayload {
    email: string;
    password: string;
}

export interface LoginUserResp {
    data: {
        statusCode: number;
        data: {
            email: string;
            name: string;
            userID: string;
        };
    };
    status: number;
    statusText: string;
}