import axios from "../config/axios.ts";
import {LoginUserPayload, LoginUserResp} from "./interfaces/auth.ts";
export const AuthApi = {

    async loginUser(loginUserPayload: LoginUserPayload) {
        return axios
            .post<LoginUserPayload, LoginUserResp>(`/auth`, loginUserPayload)
            .then((r: any) => r.data.data);
    },

};