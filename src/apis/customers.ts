import axios from "../config/axios.ts";
import {CreateCustomerResp, Customer, CustomerResp} from "./interfaces/customers.ts";
export const CustomersApi = {
    async createCustomer(customer: Customer) {
        return axios
            .post<Customer, CreateCustomerResp>(`/customers`, customer)
            .then((r: any) => r.data.data);
    },
    async updateCustomer(customer: Customer, customerID: string) {
        return axios
            .put<Customer, CreateCustomerResp>(`/customers/${customerID}`, customer)
            .then((r: any) => r.data.data);
    },
    async getAllCustomers() {
        return axios.get<any>(`/customers`).then((customerResp: CustomerResp) => customerResp.data.data);
    },
    async deleteCustomerByID(customerID: string) {
        return axios.delete<any>(`/customers/${customerID}`).then((r: any) => r.data.data);
    },
};